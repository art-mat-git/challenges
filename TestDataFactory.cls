@IsTest
public with sharing class TestDataFactory {

    private static final Integer AMOUNT_AVAILABLE_POINTS = 500;
    private static final Integer AMOUNT_AVAILABLE_MONEY  = 250;
    private static final String  USER_EMAIL_ENCODING_KEY = 'UTF-8';
    private static final String  CHALLENGE_DETAIL_PREFIX = 'Challenge Detail - ';
    private static final String  USER_FIRST_NAME_PREFIX  = 'First';
    private static final String  USER_TIME_ZONE_SID_KEY  = 'America/Los_Angeles';
    private static final String  BENEFIT_DETAIL_PREFIX   = 'Benefit Detail - ';
    private static final String  USER_LAST_NAME_PREFIX   = 'Last';
    private static final String  REJECT_REASON_PREFIX    = 'Reject reason : ';
    private static final String  OFFER_BENEFIT_PREFIX    = 'Offer Benefit - ';
    private static final String  TARGET_OBJECT_PREFIX    = 'TO - ';
    private static final String  PUBLIC_GROUP_PREFIX     = 'Public Group - ';
    private static final String  DESCRIPTION_PREFIX      = 'Description : ';
    private static final String  USER_EMAIL_POSTFIX      = '@gmail.com';
    private static final String  STANDARD_USER_NAME      = 'System Administrator';
    private static final String  USER_LANGUAGE_KEY       = 'en_US';
    private static final String  ALPHABET                = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';

    private static Profile standardUserProfile {
        get {
            if (standardUserProfile == null) {
                standardUserProfile = [
                    SELECT Id
                    FROM Profile
                    WHERE Name = :STANDARD_USER_NAME
                    LIMIT 1
                ];
            }

            return standardUserProfile;
        }
        set;
    }

    public static Map<Id, Profile__c> createCustomProfiles(Integer quantity) {
        return createCustomProfiles(quantity, TestDataFactoryConstants.PROFILE_TYPE_USER);
    }

    public static Map<Id, Profile__c> createCustomProfiles(Integer quantity, String type) {
        Map<Id, Profile__c> profilesByIds = new Map<Id, Profile__c>();
        List<Profile__c>    newProfiles   = new List<Profile__c>();
        Map<Id, User>       usersByIds    = createUsers(quantity, type);

        for (User orgUser : usersByIds.values()) {
            Integer spentPoints = getRandomNumber(AMOUNT_AVAILABLE_POINTS);

            newProfiles.add(new Profile__c(
                Name             = getFullName(orgUser.FirstName, orgUser.LastName),
                User__c          = orgUser.Id,
                Type__c          = type,
                Earned_points__c = getIncreasedPoints(spentPoints),
                Spent_points__c  = spentPoints,
                Hire_Date__c     = getHireDate()
            ));
        }

        if (newProfiles.size() > 0) {
            insert newProfiles;

            profilesByIds.putAll(newProfiles);
        }

        return profilesByIds;
    }

    public static Map<Id, Benefit_Detail__c> createBenefitDetails(Integer quantity) {
        return createBenefitDetails(quantity, TestDataFactoryConstants.BENEFIT_DETAIL_STATUS_DRAFT);
    }

    public static Map<Id, Benefit_Detail__c> createBenefitDetails(Integer quantity, String status) {
        Map<Id, Benefit_Detail__c> benefitDetailsByIds = new Map<Id, Benefit_Detail__c>();
        List<Benefit_Detail__c>    newBenefitDetails   = new List<Benefit_Detail__c>();
        List<Profile__c>           approvers           = createCustomProfiles(quantity, TestDataFactoryConstants.PROFILE_TYPE_HR_MANAGER).values();

        for (Profile__c approver  : approvers) {
            newBenefitDetails.add(new Benefit_Detail__c(
                Label__c        = BENEFIT_DETAIL_PREFIX + getPostfix(),
                Approver__c     = approver.Id,
                Status__c       = status,
                Price_in_USD__c = getRandomValue(AMOUNT_AVAILABLE_MONEY),
                Description__c  = DESCRIPTION_PREFIX + getPostfix()
            ));
        }

        if (newBenefitDetails.size() > 0) {
            insert newBenefitDetails;

            benefitDetailsByIds.putAll(newBenefitDetails);
        }

        return benefitDetailsByIds;
    }

    public static Map<Id, Benefit__c> createBenefits(Integer quantity) {
        return createBenefits(createBenefitDetails(quantity, TestDataFactoryConstants.BENEFIT_DETAIL_STATUS_APPROVED).values());
    }

    public static Map<Id, Benefit__c> createBenefits(List<Benefit_Detail__c> benefitDetails) {
        Map<Id, Benefit__c> benefitsByIds = new Map<Id, Benefit__c>();
        List<Benefit__c>    newBenefits   = new List<Benefit__c>();

        for (Benefit_Detail__c benefitDetail : benefitDetails) {
            newBenefits.add(new Benefit__c(
                Benefit_Detail__c = benefitDetail.Id,
                Approver__c       = benefitDetail.Approver__c
            ));
        }

        if (newBenefits.size() > 0) {
            insert newBenefits;

            benefitsByIds.putAll(newBenefits);
        }

        return benefitsByIds;
    }

    public static Map<Id, User_Benefit__c> createUserBenefits(Integer quantity) {
        return createUserBenefits(createBenefits(quantity).values(), TestDataFactoryConstants.USER_BENEFIT_STATUS_WAITING_FOR_APPROVAL);
    }

    public static Map<Id, User_Benefit__c> createUserBenefits(Integer quantity, String status) {
        return createUserBenefits(createBenefits(quantity).values(), status);
    }

    public static Map<Id, User_Benefit__c> createUserBenefits(List<Benefit__c> benefits, String status) {
        Map<Id, User_Benefit__c> userBenefitsByIds = new Map<Id, User_Benefit__c>();
        List<User_Benefit__c>    newUserBenefits   = new List<User_Benefit__c>();
        Integer                  quantityBenefits  = benefits.size();
        List<Profile__c>         profiles          = createCustomProfiles(quantityBenefits, TestDataFactoryConstants.PROFILE_TYPE_USER).values();

        for (Integer i = 0; i < quantityBenefits; i++) {
            User_Benefit__c newUserBenefit = new User_Benefit__c(
                Benefit__c  = benefits[i].Id,
                Approver__c = benefits[i].Approver__c,
                Profile__c  = profiles[i].Id,
                Status__c   = status
            );

            if (status == TestDataFactoryConstants.USER_BENEFIT_STATUS_REJECTED) {
                newUserBenefit.Reasons_For_Reject__c = REJECT_REASON_PREFIX + getPostfix();
            } else {
                Integer spentPoints = (Integer)profiles[i].Spent_points__c;

                newUserBenefit.Arrival_date__c = getArrivalDate();
                newUserBenefit.Spent_points__c = spentPoints;
                newUserBenefit.Price__c        = getRandomValue(AMOUNT_AVAILABLE_MONEY);
            }

            newUserBenefits.add(newUserBenefit);
        }

        if (newUserBenefits.size() > 0) {
            insert newUserBenefits;

            userBenefitsByIds.putAll(newUserBenefits);
        }

        return userBenefitsByIds;
    }

    public static Map<Id, Group> createPublicGroups(Integer quantity) {
        Map<Id, Group> groupsByIds = new Map<Id, Group>();
        List<Group>    newGroups   = new List<Group>();

        for (Integer i = 0; i < quantity; i++) {
            newGroups.add(new Group(
                Name = PUBLIC_GROUP_PREFIX + getPostfix()
            ));
        }

        if (newGroups.size() > 0) {
            insert newGroups;

            groupsByIds.putAll(newGroups);
        }

        return groupsByIds;
    }

    public static Map<Id, Challenge_Detail__c> createChallengeDetails(Integer quantity) {
        return createChallengeDetails(quantity, TestDataFactoryConstants.CHALLENGE_DETAIL_STATUS_DRAFT, createPublicGroups(quantity).values());
    }

    public static Map<Id, Challenge_Detail__c> createChallengeDetails(Integer quantity, String status) {
        return createChallengeDetails(quantity, status, createPublicGroups(quantity).values());
    }

    public static Map<Id, Challenge_Detail__c> createChallengeDetails(Integer quantity, String status, List<Group> groups) {
        Map<Id, Challenge_Detail__c> challengeDetailsByIds = new Map<Id, Challenge_Detail__c>();
        List<Challenge_Detail__c>    newChallengeDetails   = new List<Challenge_Detail__c>();
        List<Profile__c>             approvers             = createCustomProfiles(quantity, TestDataFactoryConstants.PROFILE_TYPE_HR_MANAGER).values();

        for (Profile__c approver : approvers) {
            Date startDate  = getChallengeDetailDate(Date.today());
            Date finishDate = getChallengeDetailDate(startDate);

            Challenge_Detail__c newChallengeDetail = new Challenge_Detail__c(
                Label__c       = CHALLENGE_DETAIL_PREFIX + getPostfix(),
                Status__c      = status,
                Approver__c    = approver.Id,
                Start_date__c  = startDate,
                Finish_Date__c = finishDate,
                Points__c      = getRandomValue(AMOUNT_AVAILABLE_POINTS),
                idOffices__c   = getOfficesIds(groups)
            );

            if (status == TestDataFactoryConstants.CHALLENGE_DETAIL_STATUS_REJECTED) {
                newChallengeDetail.Rejection_reason__c = REJECT_REASON_PREFIX + getPostfix();
            }

            newChallengeDetails.add(newChallengeDetail);
        }

        if (newChallengeDetails.size() > 0) {
            insert newChallengeDetails;

            challengeDetailsByIds.putAll(newChallengeDetails);
        }

        return challengeDetailsByIds;
    }

    public static Map<Id, Challenge__c> createChallenges(Integer quantity) {
        return createChallenges(createChallengeDetails(quantity, TestDataFactoryConstants.CHALLENGE_DETAIL_STATUS_APPROVED).values(), true);
    }

    public static Map<Id, Challenge__c> createChallenges(List<Challenge_Detail__c> challengeDetails) {
        return createChallenges(challengeDetails, true);
    }

    public static Map<Id, Challenge__c> createChallenges(List<Challenge_Detail__c> challengeDetails, Boolean isActive) {
        Map<Id, Challenge__c> challengesByIds = new Map<Id, Challenge__c>();
        List<Challenge__c>    newChallenges   = new List<Challenge__c>();

        for (Challenge_Detail__c challengeDetail : challengeDetails) {
            newChallenges.add(new Challenge__c(
                Challenge_Detail__c = challengeDetail.Id,
                Approver__c         = challengeDetail.Approver__c,
                Active__c           = isActive
            ));
        }

        if (newChallenges.size() > 0) {
            insert newChallenges;

            challengesByIds.putAll(newChallenges);
        }

        return challengesByIds;
    }

    public static Map<Id, User_Challenge__c> createUserChallenges(Integer quantity) {
        return createUserChallenges(createChallenges(quantity).values(), TestDataFactoryConstants.USER_CHALLENGE_STATUS_IN_PROGRESS);
    }

    public static Map<Id, User_Challenge__c> createUserChallenges(Integer quantity, String status) {
        return createUserChallenges(createChallenges(quantity).values(), status);
    }

    public static Map<Id, User_Challenge__c> createUserChallenges(List<Challenge__c> challenges) {
        return createUserChallenges(challenges, TestDataFactoryConstants.USER_CHALLENGE_STATUS_IN_PROGRESS);
    }

    public static Map<Id, User_Challenge__c> createUserChallenges(List<Challenge__c> challenges, String status) {
        Map<Id, User_Challenge__c> userChallengesByIds = new Map<Id, User_Challenge__c>();
        List<User_Challenge__c>    newUserChallenges   = new List<User_Challenge__c>();
        List<Profile__c>           profiles            = createCustomProfiles(challenges.size(), TestDataFactoryConstants.PROFILE_TYPE_USER).values();

        for (Integer i = 0; i < challenges.size(); i++) {
            User_Challenge__c newUserChallenge = new User_Challenge__c(
                Challenge__c     = challenges[i].Id,
                Approver__c      = challenges[i].Approver__c,
                Profile__c       = profiles[i].Id,
                Status__c        = status,
                Earned_points__c = getRandomValue(AMOUNT_AVAILABLE_POINTS)
            );

            if (status == TestDataFactoryConstants.USER_CHALLENGE_STATUS_REJECTED) {
                newUserChallenge.Reasons_For_Reject__c = REJECT_REASON_PREFIX + ' ' + getPostfix();
                newUserChallenge.WasRejected__c        = true;
                newUserChallenge.NumberOfReject__c     = 1;
            }

            newUserChallenges.add(newUserChallenge);
        }

        if (newUserChallenges.size() > 0) {
            insert newUserChallenges;

            userChallengesByIds.putAll(newUserChallenges);
        }

        return userChallengesByIds;
    }

    public static Map<Id, Offer_Benefit__c> createOfferBenefits(Integer quantity) {
        return createOfferBenefits(quantity, TestDataFactoryConstants.OFFER_BENEFIT_STATUS_WAITING);
    }

    public static Map<Id, Offer_Benefit__c> createOfferBenefits(Integer quantity, String status) {
        Map<Id, Offer_Benefit__c> offerBenefitsByIds = new Map<Id, Offer_Benefit__c>();
        List<Offer_Benefit__c>    newOfferBenefits   = new List<Offer_Benefit__c>();
        List<Profile__c>          approvers          = createCustomProfiles(quantity, TestDataFactoryConstants.PROFILE_TYPE_HR_MANAGER).values();
        List<Profile__c>          profiles           = createCustomProfiles(quantity, TestDataFactoryConstants.PROFILE_TYPE_USER).values();

        for (Integer i = 0; i < quantity; i++) {
            Offer_Benefit__c newOfferBenefit = new Offer_Benefit__c(
                Name           = OFFER_BENEFIT_PREFIX + ' ' + getPostfix(),
                Approver__c    = approvers[i].Id,
                Profile__c     = profiles[i].Id,
                Status__c      = status,
                Price__c       = getRandomValue(AMOUNT_AVAILABLE_MONEY),
                Description__c = DESCRIPTION_PREFIX + getPostfix()
            );

            if (status == TestDataFactoryConstants.OFFER_BENEFIT_STATUS_REJECTED) {
                newOfferBenefit.Rejection_reason__c = REJECT_REASON_PREFIX + getPostfix();
            }

            newOfferBenefits.add(newOfferBenefit);
        }

        if (newOfferBenefits.size() > 0) {
            insert newOfferBenefits;

            offerBenefitsByIds.putAll(newOfferBenefits);
        }

        return offerBenefitsByIds;
    }

    public static Map<Id, User_Logs__c> createUserLogs(Integer quantity) {
        return createUserLogs(quantity, TestDataFactoryConstants.USER_LOGS_TYPE_INFO);
    }

    public static Map<Id, User_Logs__c> createUserLogs(Integer quantity, String type) {
        Map<Id, User_Logs__c> userLogsByIds = new Map<Id, User_Logs__c>();
        List<User_Logs__c>    newUserLogs   = new List<User_Logs__c>();
        List<Profile__c>      profiles      = createCustomProfiles(quantity, TestDataFactoryConstants.PROFILE_TYPE_USER).values();

        for (Profile__c profile : profiles) {
            newUserLogs.add(new User_Logs__c(
                Active_User__c   = profile.Id,
                Type__c          = type,
                Date_Of_Log__c   = Datetime.now(),
                Description__c   = DESCRIPTION_PREFIX + getPostfix(),
                Target_Object__c = TARGET_OBJECT_PREFIX + getPostfix()
            ));
        }

        if (newUserLogs.size() > 0) {
            insert newUserLogs;

            userLogsByIds.putAll(newUserLogs);
        }

        return userLogsByIds;
    }

    public static Map<Id, Month_Report_Benefits__c> createMonthReports(Integer quantity) {
        Map<Id, Month_Report_Benefits__c> monthReportsByIds = new Map<Id, Month_Report_Benefits__c>();
        List<Month_Report_Benefits__c>    newMonthReports   = new List<Month_Report_Benefits__c>();
        List<User>                        users             = createUsers(quantity, TestDataFactoryConstants.PROFILE_TYPE_USER).values();

        for (User user : users) {
            newMonthReports.add(new Month_Report_Benefits__c(
                Accountant__c   = user.Id,
                HR__c           = user.Id,
                OwnerId         = user.Id,
                Total_Amount__c = getRandomValue(AMOUNT_AVAILABLE_MONEY)
            ));
        }

        if (newMonthReports.size() > 0) {
            insert newMonthReports;

            monthReportsByIds.putAll(newMonthReports);
        }

        return monthReportsByIds;
    }

    private static Map<Id, User> createUsers(Integer quantity, String type) {
        Map<Id, User> usersByIds = new Map<Id, User>();
        List<User>    newUsers   = new List<User>();

        for (Integer i = 0; i < quantity; i++) {
            String firstName = USER_FIRST_NAME_PREFIX + getPostfix();
            String lastName  = USER_LAST_NAME_PREFIX + getPostfix();
            String alias     = getAlias(firstName, lastName);

            newUsers.add(new User(
                    FirstName         = firstName,
                    LastName          = lastName,
                    Alias             = alias,
                    Email             = getUserEmail(firstName, lastName, type),
                    Username          = getUserName(firstName, lastName, type),
                    CompanyName       = getCompanyName(alias),
                    ProfileId         = standardUserProfile.Id,
                    LanguageLocaleKey = USER_LANGUAGE_KEY,
                    EmailEncodingKey  = USER_EMAIL_ENCODING_KEY,
                    TimeZoneSidKey    = USER_TIME_ZONE_SID_KEY,
                    LocaleSidKey      = USER_LANGUAGE_KEY
            ));
        }

        if (newUsers.size() > 0) {
            insert newUsers;

            usersByIds.putAll(newUsers);
        }

        return usersByIds;
    }

    private static String getPostfix() {
        Integer quantity = getRandomNumber(10);
        String  postfix  = '';

        quantity = quantity < 3 ? 3 : quantity;

        for (Integer i = 0; i < quantity; i++) {
            Integer letterNumber = getRandomNumber(ALPHABET.length() - 1);

            postfix += ALPHABET.substring(letterNumber, letterNumber + 1);
        }

        return postfix;
    }

    private static Integer getRandomNumber(Integer multiplier) {
        Decimal randomNumber = Math.random() * multiplier;

        return (Integer)randomNumber;
    }

    private static String getAlias(String firstName, String lastName) {
        firstName = deleteWhitespaces(firstName);
        lastName  = deleteWhitespaces(lastName);

        return firstName.substring(firstName.length() - 1, firstName.length()) + lastName.substring(lastName.length() - 3, lastName.length());
    }

    private static Integer getIncreasedPoints(Integer points) {
        Integer multiplier = getRandomNumber(10);

        multiplier = multiplier == 0 ? 1 : multiplier;
        points     = points     == 0 ? 1 : points;

        return points * multiplier;
    }

    private static String getUserEmail(String firstName, String lastName, String type) {
        return deleteWhitespaces(firstName + '.' + lastName + '.' + type + USER_EMAIL_POSTFIX);
    }

    private static String getUserName(String firstName, String lastName, String type) {
        return deleteWhitespaces(firstName + '.' + lastName + '@' + type + '.com');
    }

    private static String deleteWhitespaces(String incomingString) {
        return incomingString.contains(' ') ? incomingString.deleteWhitespace() : incomingString;
    }

    private static String getCompanyName(String alias) {
        return alias.toUpperCase() + ' CONSULTING';
    }

    private static String getFullName(String firstName, String lastName) {
        return firstName + ' ' + lastName;
    }

    private static Date getHireDate() {
        return Date.today().addYears(-(getRandomNumber(15)));
    }

    private static Integer getRandomValue(Integer multiplier) {
        Integer price = getRandomNumber(multiplier);

        return price == 0 ? 1 : price;
    }

    private static Date getArrivalDate() {
        Integer quantityMonths = getRandomNumber(5);

        quantityMonths = quantityMonths == 0 ? 1 : quantityMonths;

        return Date.today().addMonths(quantityMonths);
    }

    private static Date getChallengeDetailDate(Date incomingDate) {
        Integer addend = getRandomNumber(10);

        return incomingDate.addDays(addend + 1);
    }

    private static String getOfficesIds(List<Group> groups) {
        Set<Id> officesIdsSet     = new Set<Id>();
        Integer quantityIdOffices = getRandomNumber(10);

        quantityIdOffices = quantityIdOffices == 0 ? 1 : quantityIdOffices;

        for (Integer i = 0; i < quantityIdOffices; i++) {
            Integer groupNumber = getRandomNumber(groups.size());

            officesIdsSet.add(groups[groupNumber].Id);
        }

        String   officesIdsSetJSON = JSON.serialize(officesIdsSet);
        List<Id> officesIdsList    = (List<Id>)JSON.deserialize(officesIdsSetJSON, List<Id>.class);
        String   officesIds        = String.join(officesIdsList, ';');

        return officesIds;
    }
}