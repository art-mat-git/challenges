@IsTest
public with sharing class TestDataFactoryConstants {
    public static final String PROFILE_TYPE_USER          = 'User';
    public static final String PROFILE_TYPE_MANAGER       = 'Manager';
    public static final String PROFILE_TYPE_ADMINISTRATOR = 'Administrator';
    public static final String PROFILE_TYPE_HR_MANAGER    = 'HR Manager';
    public static final String PROFILE_TYPE_HRD           = 'HRD';

    public static final String BENEFIT_DETAIL_STATUS_DRAFT                = 'Draft';
    public static final String BENEFIT_DETAIL_STATUS_WAITING_FOR_APPROVAL = 'Waiting for approval';
    public static final String BENEFIT_DETAIL_STATUS_APPROVED             = 'Approved';
    public static final String BENEFIT_DETAIL_STATUS_REJECTED             = 'Rejected';

    public static final String USER_BENEFIT_STATUS_DELAYED              = 'Delayed';
    public static final String USER_BENEFIT_STATUS_WAITING_FOR_APPROVAL = 'Waiting for approval';
    public static final String USER_BENEFIT_STATUS_WAITING_FOR_RECEIVAL = 'Waiting for receival';
    public static final String USER_BENEFIT_STATUS_RECEIVED             = 'Received';
    public static final String USER_BENEFIT_STATUS_REJECTED             = 'Rejected';
    public static final String USER_BENEFIT_STATUS_DRAFT                = 'Draft';
    public static final String USER_BENEFIT_STATUS_APPROVED             = 'Approved';

    public static final String CHALLENGE_DETAIL_STATUS_DRAFT                = 'Draft';
    public static final String CHALLENGE_DETAIL_STATUS_WAITING_FOR_APPROVAL = 'Waiting for approval';
    public static final String CHALLENGE_DETAIL_STATUS_APPROVED             = 'Approved';
    public static final String CHALLENGE_DETAIL_STATUS_REJECTED             = 'Rejected';

    public static final String USER_CHALLENGE_STATUS_APPROVED    = 'Approved';
    public static final String USER_CHALLENGE_STATUS_HIDDEN      = 'Hidden';
    public static final String USER_CHALLENGE_STATUS_WAITING     = 'Waiting';
    public static final String USER_CHALLENGE_STATUS_IN_PROGRESS = 'In progress';
    public static final String USER_CHALLENGE_STATUS_REJECTED    = 'Rejected';
    public static final String USER_CHALLENGE_STATUS_FAILED      = 'Failed';

    public static final String OFFER_BENEFIT_STATUS_WAITING  = 'Waiting';
    public static final String OFFER_BENEFIT_STATUS_APPROVED = 'Approved';
    public static final String OFFER_BENEFIT_STATUS_REJECTED = 'Rejected';

    public static final String USER_LOGS_TYPE_INFO = 'Info';
    public static final String USER_LOGS_TYPE_SEND = 'Send';
    public static final String USER_LOGS_TYPE_BUY  = 'Buy';
    public static final String USER_LOGS_TYPE_GIFT = 'Gift';
}